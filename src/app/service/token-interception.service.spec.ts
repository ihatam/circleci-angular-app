import { TestBed } from '@angular/core/testing';

import { TokenInterceptionService } from './token-interception.service';

describe('TokenInterceptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TokenInterceptionService = TestBed.get(TokenInterceptionService);
    expect(service).toBeTruthy();
  });
});
