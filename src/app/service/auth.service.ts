import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl="http://circleci-prod.vtxxuxpxcf.us-east-1.elasticbeanstalk.com/"
  constructor(private _http: HttpClient,private _router: Router) { }
  addUsers(user){
    return this._http.post<any>(`${this.apiUrl}${'singup/'}`,user)
    .pipe(catchError(this.errorHandler))
  }
  login(userLogin ){
    return this._http.post<any>(`${this.apiUrl}${'login/'}`,userLogin)
    .pipe(catchError(this.errorHandler))
  }
  errorHandler(error: HttpErrorResponse){
    return throwError(error)
  }
  loggedIn(){
    return !!localStorage.getItem('token')
  }
  getToken(){
    return localStorage.getItem('token')
  }
  logoutUser(){
    localStorage.removeItem('token')
    this._router.navigate(['/login'])
  }
}
