import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-gst-login',
  templateUrl: './gst-login.component.html',
  styleUrls: ['./gst-login.component.sass']
})
export class GstLoginComponent implements OnInit {

  userModel = new User("","admin@admin.com",
  "admin","","","")
  submitted = false
  errMsg
  valideUser = false;
  onSubmit() {
    this.submitted = true;
    return this.authService.login(this.userModel).subscribe(data => {
       console.log('Succes',data)
       localStorage.setItem('token',data.token)
       this.router.navigate(['/users'])
     }, err => {
       this.errMsg = err.statusText
     })
  }
  constructor(private authService: AuthService ,private router: Router ) { 
  }

  ngOnInit() {
  }
}
