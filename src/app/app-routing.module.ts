import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GstLoginComponent } from './modules/gst-login/gst-login.component';
import { GstUsersComponent } from './modules/gst-users/gst-users.component';
import { GstSignUpComponent } from './modules/gst-sign-up/gst-sign-up.component';
import { GstEditUsersComponent } from './modules/gst-edit-users/gst-edit-users.component';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path:'',
    redirectTo:'/login',
    pathMatch:'full'
  },
  {
    path:'login',
    component: GstLoginComponent
  },{
    path:'users',
    component: GstUsersComponent,
    canActivate: [AuthGuard],
  },{
    path:'singup',
    component:GstSignUpComponent
  },{
    path: 'users/:_id',
    component: GstEditUsersComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
