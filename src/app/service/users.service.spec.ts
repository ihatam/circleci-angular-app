import { HttpClientModule } from '@angular/common/http';

import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UsersService } from './users.service';

describe('UsersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        HttpClientModule,
      ],
      providers: [
        UsersService,
      ],
    });
  });

  it('should be created', inject([HttpTestingController, UsersService],
      (httpMock: HttpTestingController, usersService: UsersService) => {
        expect(usersService).toBeTruthy();
      }
    )
  );
});
