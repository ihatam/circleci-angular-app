import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { UsersService } from 'src/app/service/users.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-gst-users',
  templateUrl: './gst-users.component.html',
  styleUrls: ['./gst-users.component.sass'],
})
export class GstUsersComponent implements OnInit {
  title = 'material-ng';
  errMsg: any
  userList: User[] | undefined
  constructor(private usersService: UsersService, private router: Router ) { 
  }
  ngOnInit() {
    return this.usersService.getUsers().subscribe(data => {
        console.log('Succes',data)
        this.userList = data
      }, err => {
        this.errMsg = err.statusText
      })
  }
  deleteUser(userId: any){
    this.usersService.deleteUser(userId).subscribe(data => {
      console.log('Succes',data)
      this.ngOnInit()
    }, err => {
      this.errMsg = err.statusText
    })
  }
}