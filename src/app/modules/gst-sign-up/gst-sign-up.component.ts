import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-gst-sign-up',
  templateUrl: './gst-sign-up.component.html',
  styleUrls: ['./gst-sign-up.component.sass']
})
export class GstSignUpComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router ) { 
  }
  /* userModelStruct  
  {
    _id: String,
  email:String,
  password: String,
  first_name: String,
  last_name:String,
  status: String 
  }
  */
  title = 'SimpleLoginApp';
  userModel = new User("5c3662c110d15339a907103a","unon@ferion.lima.com",
  "Marquis","Jack","LOPIU","off")
  submitted = false
  errMsg
  ngOnInit() {
  }
  onSubmit() {
     this.submitted = true;
     return this.authService.addUsers(this.userModel).subscribe(data => {
        console.log('Succes',data)
        //localStorage.setItem('token',data.token)
        this.router.navigate(['/login'])
      }, err => {
        this.errMsg = err.statusText
      })
  }
}