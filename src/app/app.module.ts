import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GstLoginComponent } from './modules/gst-login/gst-login.component';
import { GstUsersComponent } from './modules/gst-users/gst-users.component';
import { GstEditUsersComponent } from './modules/gst-edit-users/gst-edit-users.component';
import { GstSignUpComponent } from './modules/gst-sign-up/gst-sign-up.component';
import { UsersService } from './service/users.service';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthService } from './service/auth.service';
import { AuthGuard } from './guard/auth.guard';
import { TokenInterceptionService } from './service/token-interception.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { MaterialModule } from './materiel';
@NgModule({
  declarations: [
    AppComponent,
    GstLoginComponent,
    GstUsersComponent,
    GstEditUsersComponent,
    GstSignUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [UsersService,AuthService,AuthGuard,{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptionService,
    multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
