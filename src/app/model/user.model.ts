export class User{
    constructor(
        public _id: String,
        public email:String,
        public password: String,
        public first_name: String,
        public last_name:String,
        public status: String,
    ){ 
     }  
}