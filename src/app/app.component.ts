import { Component } from '@angular/core';
import { User } from './user';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'circleci-angular-app'
  constructor(private _authService: AuthService){}
}
