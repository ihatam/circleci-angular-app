import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/service/users.service';

@Component({
  selector: 'app-gst-edit-users',
  templateUrl: './gst-edit-users.component.html',
  styleUrls: ['./gst-edit-users.component.sass']
})
export class GstEditUsersComponent implements OnInit {
  userId
  userModel
  errMsg
  submitted = false
  constructor(private route: ActivatedRoute,private usersService: UsersService, private router: Router) {
    /* const id = this.route.snapshot.paramMap.get('_id');
    this.userId = id*/
  }
  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('_id');
    return this.usersService.getUsersId(id).subscribe(data => {
      console.log('Succes',data)
      this.userModel = data
    }, err => {
      this.errMsg = err.statusText
    })
  }
  onSubmit(){
    const id = this.route.snapshot.paramMap.get('_id');
    this.submitted = true;
    return this.usersService.updateUser(id,this.userModel).subscribe(data => {
       console.log('Succes',data)
       this.router.navigate(['/users'])
     }, err => {
       this.errMsg = err.statusText
     })
  }
}
