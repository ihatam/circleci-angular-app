import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../model/user.model';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  apiUrl="http://circleci-prod.vtxxuxpxcf.us-east-1.elasticbeanstalk.com/"
  constructor(private _http: HttpClient) { }
  getUsers(){
    return this._http.get<User[]>(`${this.apiUrl}${'users'}`)
    .pipe(catchError(this.errorHandler))
  }
  getUsersId(userId){
    return this._http.get<User>(`${this.apiUrl}${'users/'}${userId}`)
    .pipe(catchError(this.errorHandler))
  }
  deleteUser(userId){
    return this._http.delete<User>(`${this.apiUrl}${'users/'}${userId}`)
    .pipe(catchError(this.errorHandler))
  }
  updateUser(userId,user){
    return this._http.put(`${this.apiUrl}${'users/'}${userId}`,user)
    .pipe(catchError(this.errorHandler))
  }
  errorHandler(error: HttpErrorResponse){
    return throwError(error)
  }
}
