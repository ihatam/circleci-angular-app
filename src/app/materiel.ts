import {MatButtonModule,MatCheckboxModule} from '@angular/material'
import { NgModule } from '@angular/core';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';

@NgModule({
    imports:[MatButtonModule,MatCheckboxModule,MatDividerModule,MatListModule,
        MatTableModule],
    exports: [MatButtonModule,MatCheckboxModule,MatDividerModule,MatListModule,
        MatTableModule]
})
export class MaterialModule {}